import React, { Component } from 'react';
import './App.css';
import PhoneBooks from "./containers/PhoneBooks/PhoneBooks";
import { NavLink, Route, Switch } from "react-router-dom";
import AddContact from "./containers/AddContact/AddContact";

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-top">
          <h3>Contacts</h3>
          <button>
            <NavLink to="/add-contact" exact>Add new contact</NavLink>
          </button>
        </div>

        <Switch>
          <Route path="/" exact component={ PhoneBooks } />
          <Route path="/add-contact" component={ AddContact } />
          <Route render={() => <h3>Not found</h3>} />
        </Switch>
      </div>
    );
  }
}

export default App;
