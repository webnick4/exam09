import React, { Component } from 'react';
import { NavLink, Route, Switch } from "react-router-dom";
import './ContactForm.css';
import PhoneBooks from "../../containers/PhoneBooks/PhoneBooks";


class ContactForm extends Component {
  state = {
    contact: {
      name: '',
      phone: '',
      email: '',
      photo: ''
    }
  };

  inputChangedHandler = event => {
    const contactCopy = {...this.state.contact};
    contactCopy[event.target.name] = event.target.value;
    this.setState({contact: contactCopy});
  };

  submitHandler = event => {
    event.preventDefault();
    let contact = {...this.state.contact};
    contact.name = this.state.contact.name;
    contact.phone = this.state.contact.phone;
    contact.email = this.state.contact.email;
    contact.photo = this.state.contact.photo;
    this.props.saved(this.state.contact);
  };

  render() {
    return (
      <form className="ContactForm-form" onSubmit={this.submitHandler}>
        <div className="ContactForm-field">
          <label>Name</label>
          <input type="text" name="name" value={this.state.contact.name} onChange={this.inputChangedHandler} />
        </div>
        <div className="ContactForm-field">
          <label>Phone</label>
          <input type="tel" name="phone" value={this.state.contact.phone} onChange={this.inputChangedHandler} />
        </div>
        <div className="ContactForm-field">
          <label>EMail</label>
          <input type="email" name="email" value={this.state.contact.email} onChange={this.inputChangedHandler} />
        </div>
        <div className="ContactForm-field">
          <label>Photo</label>
          <input type="url" name="photo" value={this.state.contact.photo} onChange={this.inputChangedHandler} />
        </div>

        <div className="ContactForm-field">
          <button>Save</button>
          <button>
            <NavLink to="/" exact>Back to contacts</NavLink>
          </button>
          <Switch>
            <Route path="/" exact component={ PhoneBooks } />
          </Switch>
        </div>
      </form>
    );
  }
}

export default ContactForm;