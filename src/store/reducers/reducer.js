import * as actionTypes from '../actions/actionTypes';

const initialState = {
  contact: {
    name: '',
    phone: '',
    email: '',
    photo: ''
  }
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ADD_CONTACT:
      return {};
    default:
      return state;
  }
};

export default reducer;