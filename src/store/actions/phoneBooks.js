import * as actionTypes from './actionTypes';

export const addContact = contactName => {
  return {type: actionTypes.ADD_CONTACT, contactName};
};