import React, { Component } from 'react';
import axios from 'axios';
import './PhoneBooks.css';
import Spinner from "../../components/UI/Spinner/Spinner";

class PhoneBooks extends  Component {
  state = {
    contacts: [],
    loading: true
  };

  componentDidMount() {
    axios.get('/contacts.json').then(response => {
      let contacts = Object.keys(response.data).map(key => {
        return {...response.data[key], id: key};
      });
      this.setState({contacts, loading: false});
    }).catch(() => {
      this.setState({loading: false});
    });
  }

  render() {
    let contacts = this.state.contacts.map(contact => {
      console.log(contact);
      return (
          <div className="PhoneList" key={contact.id}>
            <img src={ contact.photo } />
            <div>{ contact.name }</div>
          </div>
        )});

    if (this.state.loading) {
        contacts = <Spinner />
    }
    return (
      <div className="PhoneBooks">
        { contacts }
      </div>
    );
  }
}

export default PhoneBooks;