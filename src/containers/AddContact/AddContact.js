import React, { Component } from 'react';
import axios from 'axios';
import './AddContact.css';
import Spinner from "../../components/UI/Spinner/Spinner";
import ContactForm from "../../components/ContactForm/ContactForm";

class AddContact extends Component {
  state = {
    loading: false
  };

  postAddHandler = contact => {
    this.setState({loading: true});
    axios.post('/contacts.json', contact);
    this.setState({loading: false});
    this.props.history.push('/');
  };

  render() {
    let form = <ContactForm saved={this.postAddHandler} />;

    if (this.state.loading) {
      form = <Spinner />;
    }

    return (
      <div className="AddContact">
        <h2>Add new Contact</h2>
        { form }
      </div>
    );
  }
}

export default AddContact;